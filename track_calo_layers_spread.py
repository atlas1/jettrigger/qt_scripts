#Layer numbering:
#enum LAYER {
#    EMB1=0, EMB2, EMB3,
#    EME1, EME2, EME3,
#    HEC1, HEC2, HEC3, HEC4,
#    Tile1, Tile2, Tile3,
#    FCAL0, FCAL1, FCAL2,
#    Unknown=999
#  };

import ROOT
import sys, math

sample_file = './sample_files/test_output.txt'

hist_delta_vs_pt = ROOT.TH2F("delta_vs_pt","Difference of track position in the first and last calo layer vs. p_{T}; #Delta; p_{T}",50,0,0.2, 120, 0, 30)

dict1={}
tracks_not_found = 0

with open(sample_file, 'r') as file:

	# Skip the first line with labels   
	next(file)
	for line in file:
		evtNum = line.split()[0]
		index = line.split()[1]
		layer = line.split()[2] + "x" + line.split()[3]
		extrap_eta = float(line.split()[6])
		extrap_phi = float(line.split()[7])
		pt = float(line.split()[8])
		dict1[evtNum + "x" + index + "x" + layer] = [extrap_eta, extrap_phi, pt, evtNum, index, layer]

file.close()

print("Loaded file to the array.")

for key in dict1:
	data = dict1[key]
	if data[5] == "0x1":
		last_layer = -1
		for i in range (0, 16):
			test_key = data[3] + "x" + data[4] + "x" + str(i) + "x" + "1"
			if test_key in dict1:
				key_to_look_for = test_key
				last_layer = i
		if last_layer != -1:
			print("0 ", last_layer)
		if key_to_look_for in dict1:
			reference_data = dict1[key_to_look_for]
			delta = math.sqrt((reference_data[0] - data[0])** 2 + (reference_data[1] - data[1])**2 )
			hist_delta_vs_pt.Fill(delta, data[2]/1000)
		else:
			tracks_not_found += 1	

	if data[5] == "3x1":
		last_layer = -1
		for i in range (4, 16):
			test_key = data[3] + "x" + data[4] + "x" + str(i) + "x" + "1"
			if test_key in dict1:
				key_to_look_for = test_key
				last_layer = i
		if last_layer != -1:
			print("3", last_layer)
		if key_to_look_for in dict1:
			reference_data = dict1[key_to_look_for]
			delta = math.sqrt((reference_data[0] - data[0])** 2 + (reference_data[1] - data[1])**2 )
			hist_delta_vs_pt.Fill(delta, data[2]/1000)
		else:
			tracks_not_found += 1	
		

canvas1 = ROOT.TCanvas("Canvas1","c1",1600,1200)
canvas1.cd()
hist_delta_vs_pt.Draw("colz")
canvas1.SetLogz()
hist_delta_vs_pt.SetStats(0)
canvas1.Draw("hist")
canvas1.Print("track_calo_layers_spread.png")	


