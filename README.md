Collection of scripts that I wrote to make work on my QT a little bit easier.


# Content of this repository
This repo contains two python scripts and sample fies that can be used to test these scripts.


1. **compare_tracks_full.py** : This is the sript that I used to extract most of the data I later used to investigate the problem of track extrapolation. The script takes the input reference file containing information about all hits in all calo layers for the given data set and compares it to another input file of the same type which was produced with different configuration. This input file needs to be in the following specific format. The first line contains labels of columns and then every next line corresponds to one hit in any calo layer and contains these pieces of information (in the following order): `eventNumber track.index() layerID isEntry  track.eta() track.phi() extrap.eta() extrap.phi() track.pt()`. The script takes one or two arguments. If one is provided, it is the path to the file we want to compare. Reference file is hardcoded in the sropt by default. If one wants to change the reference file used, it can be specified by the other input parameter. The script produces a lot of output. Missing hits in individual layers and also precision of extrapolation in given layers (eta, phi differences distributions and distance in eta-phi plane between hits before and after change of configuration). It also produces two output text files. One contains information about missing hits in different layers. The other contains information about mean eta-phi distance of hits before and after configuration change. 
<br />

2.  **track_calo_layers_spread.py**: This script takes only one input file and computes distance between track entry point to the calorimeter and the last hit in the calorimeter. It assumes that track has hit either in layer 0 (EMB1 - barrel) or 3 (EME1 - endcap). Computed values are then plotted to the 2D histogram, where the other axis is track p_T.  
<br />

Both scripts can be tested on sample files that are also in this repository.
